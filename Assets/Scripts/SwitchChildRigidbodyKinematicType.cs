using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchChildRigidbodyKinematicType : MonoBehaviour
{

    private Rigidbody[] rbs;

    private void Awake()
    {
        rbs = GetComponentsInChildren<Rigidbody>();
        MakeKinematic();
    }

    public void MakeKinematic()
    {
        foreach (var rb in rbs)
        {
            rb.isKinematic = true;
        }
    }
    
    public void MakeNotKinematic()
    {
        foreach (var rb in rbs)
        {
            rb.isKinematic = false;
        }
    }
}
