using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerController))]
public class PlayerInputControls : MonoBehaviour
{
    private PlayerController _playerController;

    private void Awake()
    {
        _playerController = GetComponent<PlayerController>();
    }
    
    private void OnEnable()
    {
        TouchInputManager.Instance.TouchBeginEvent += OnTouchBegin;
        TouchInputManager.Instance.TouchProcessEvent += OnTouchProcess;
    }

    private void OnDisable()
    {
        if (TouchInputManager.Instance != null) 
        {
            TouchInputManager.Instance.TouchBeginEvent -= OnTouchBegin;
            TouchInputManager.Instance.TouchProcessEvent -= OnTouchProcess;
        }
    }

    private void OnTouchBegin(Vector3 position)
    {
        _playerController.MoveToPos(position);
        _playerController.Shoot(position);
    }
    
    private void OnTouchProcess(Vector3 position)
    {  
        _playerController.SetAim(position);
        _playerController.Shoot(position);
    }

    public void SetAim(InputAction.CallbackContext ctx)
    {
        if (ctx.performed) _playerController.SetAim(ctx.ReadValue<Vector2>());
    }

}
