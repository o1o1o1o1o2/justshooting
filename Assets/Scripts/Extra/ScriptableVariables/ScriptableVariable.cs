using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class ScriptableVariable<T> : ScriptableObject, IResetable, ISerializationCallbackReceiver where T : struct
{
    public T InitialValue;

    [NonSerialized]
    public T RuntimeValue;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    public void OnAfterDeserialize()
    {
        RuntimeValue = InitialValue;
    }
    
    public void OnBeforeSerialize()
    {
    }

#if UNITY_EDITOR
    private void OnEnable()
    {
        ResetablesDatabase.Register(this);
    }
 
    private void OnDisable()
    {
        ResetablesDatabase.UnRegister(this);
    }
    private void OnDestroy()
    {
        ResetablesDatabase.UnRegister(this);
    }
#endif
    
    public void ResetInternalState()
    {
#if UNITY_EDITOR
        OnAfterDeserialize();
#endif
    }
}
