public interface IResetable
{
    void ResetInternalState();
}