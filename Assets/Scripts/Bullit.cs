using System.Collections;
using UnityEngine;

public class Bullit : MonoBehaviour
{
    private Transform _transform;
    private Vector3 _force;

    private const string _levelTag = "Level";
    private const string _enemyTag = "Enemy";
    
    [SerializeField] 
    private ParticleSystem _hitParticles;
    
    [SerializeField] 
    private ParticleSystem _hitFleshParticles;

    public void Awake()
    {
        _transform = transform;
    }

    public void FireBullit(Vector3 direction, float speed)
    {
        _force = direction.normalized*(speed*1000);
        GetComponent<Rigidbody>().AddForce(_force);
        StartCoroutine(destroyAfterTime());
    }

    IEnumerator destroyAfterTime()
    {
        yield return  new  WaitForSeconds(1);
        Destroy(transform.gameObject); 
    }

    private void EmitParticles(ContactPoint hitPos, ParticleSystem particleSystem)
    {
        particleSystem.transform.position = hitPos.point;
        particleSystem.transform.forward = hitPos.normal;
        particleSystem.Emit(1);
    }

    private void  OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag(_enemyTag))
        {
            collision.collider.GetComponentInParent<EnemyBehaviour>().Die(collision.collider,_force, collision.contacts[0].point);
            EmitParticles(collision.contacts[0], _hitFleshParticles);
        }
        else
        {
            EmitParticles(collision.contacts[0], _hitParticles);
        }
        
        Destroy(transform.gameObject); 
    }
}
