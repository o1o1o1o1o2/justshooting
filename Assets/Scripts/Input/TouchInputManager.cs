using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine;
using UnityEngine.InputSystem;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
using TouchPhase = UnityEngine.InputSystem.TouchPhase;

[DefaultExecutionOrder(-10)]
public class TouchInputManager : Singleton<TouchInputManager>
{

    private PlayerInput _playerInput;
    public delegate void TouchBeginDelegate(Vector3 position);
    public event TouchBeginDelegate TouchBeginEvent;

    public delegate void TouchProcessDelegate(Vector3 position);
    public event TouchProcessDelegate TouchProcessEvent;

    public delegate void TouchEndedDelegate(Vector3 position);
    public event TouchEndedDelegate TouchEndedEvent;

    void Awake()
    {
        EnhancedTouchSupport.Enable();
        _playerInput = GetComponent<PlayerInput>();
    }
    
    void Update()
    {
        if (_playerInput.currentControlScheme == "Mouse")
        {
            var v = Mouse.current.position.ReadValue();
#if UNITY_EDITOR
            if (v.x < 0 || v.y < 0 || v.x > Screen.width || v.y > Screen.height) return;
#endif
            if (!PointerExt.IsPointerOverUIElement(v))
            {
                if (Mouse.current.leftButton.IsPressed(0.1f)) TouchBeginEvent?.Invoke(v);
            }
        }
        else
        {
            if (Touch.activeTouches.Count > 0)
            {
                switch (Touch.activeTouches[0].phase)
                {
                    case TouchPhase.Began:
                        if (!PointerExt.IsPointerOverUIElement(Touch.activeTouches[0].screenPosition)) TouchBeginEvent?.Invoke(Touch.activeTouches[0].screenPosition);
                        break;
                    case TouchPhase.Moved:
                    case TouchPhase.Stationary:
                        TouchProcessEvent?.Invoke(Touch.activeTouches[0].screenPosition);
                        break;
                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        TouchEndedEvent?.Invoke(Touch.activeTouches[0].screenPosition);
                        break;
                }
            }
        }
    }
}
