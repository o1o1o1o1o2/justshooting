using System.Collections;
using Fly.Utils;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations.Rigging;


[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Camera camera;

    private AudioSource _audioSource;
    
    [SerializeField]
    private GameObject bullitPrefab;

    [SerializeField] 
    private FloatReference bullitSpeed;

    [SerializeField] 
    private FloatReference movementSpeed;
    
    [SerializeField] 
    private FloatReference shootingSpeed;
    
    [SerializeField] 
    private float recoilTime;

    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    
    [SerializeField]
    private MultiAimConstraint gunAimConstraint;
    [SerializeField]
    private MultiRotationConstraint gunRecoilRotationConstraint;

    [SerializeField]
    private Transform enemyTr;
    
    [SerializeField]
    private Transform playerHeadTr;
    [SerializeField]
    private Transform gunBarrel;

    private const string AimingZoneTag = "AimingZone";
    private const string EnemyTag = "Enemy";
    private const string ShootingAnimation = "shooting";

    private int _idleId;
    private int _runingId;
    private int _aimingId;
    private int _shootingId;
    
    private bool _isAiming;
    private bool _shooting;
    private bool _canShoot = false;

    [SerializeField] 
    private Rig twisterRig;

    [SerializeField]
    private Transform aimGOTransform;

    [SerializeField] 
    private ParticleSystem[] fireParticles;

    public void SetIdle()
    {
        _canShoot = false;
        _shooting = false;
        _isAiming = false;
        _animator.SetBool(_shootingId, false);
        _animator.SetBool(_aimingId, false);
        _animator.SetBool(_runingId, false);
        twisterRig.weight = 0;
        _navMeshAgent.enabled = false;
        _navMeshAgent.enabled = true;
        gunAimConstraint.weight = 0;
    }


    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();

        _navMeshAgent.speed *= movementSpeed;
        _animator.SetFloat("ShootingMultiplier", shootingSpeed);
        _animator.SetFloat("RunningMultiplier", movementSpeed);

        _runingId = Animator.StringToHash("run");
        _idleId = Animator.StringToHash("idle");
        _aimingId = Animator.StringToHash("aiming");
        _shootingId = Animator.StringToHash("shooting");
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (enabled && other.CompareTag(AimingZoneTag))
        {
            StartCoroutine(CheckCanShoot());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(AimingZoneTag))
        {
            _animator.SetBool(_aimingId, false);
            _isAiming = false;
        }
    }
    
    public void MoveToPos(Vector2 targetScreenPos)
    {
        if (!_isAiming)
        {
            camera.ScreenPointToHitWorldPosition(targetScreenPos, out Vector3 hitPos);
            _navMeshAgent.isStopped = false;
            _navMeshAgent.SetDestination(hitPos);
            _animator.SetBool(_aimingId, false);
            _animator.SetBool(_idleId, false);
            _animator.SetBool(_runingId, true);

            StartCoroutine(Run());
        }
    }

    public void SetAim(Vector2 position)
    {
        if (_isAiming)
        {
            camera.ScreenPointToHitWorldPosition(position, out Vector3 hit);
            aimGOTransform.position = hit;
        }
    }
    
    public void Shoot(Vector2 targetScreenPos)
    {
        if (_canShoot && !_shooting)
        {
            camera.ScreenPointToHitWorldPosition(targetScreenPos, out Vector3 targetPos);

            var gunBarrelPos = gunBarrel.position;
            
            var bullit = Instantiate(bullitPrefab,gunBarrelPos, Quaternion.identity).GetComponent<Bullit>();
            bullit.gameObject.SetActive(true);
    
            bullit.FireBullit(targetPos - gunBarrelPos, bullitSpeed);

            _animator.SetBool(_shootingId, true);
            _shooting = true;
            
            EmitParticles();
            
            StartCoroutine(WaitForShootingAnimationEnds());
            
            _audioSource.Play(0);

         
        }
    }

    private void EmitParticles()
    {
        foreach (var particles in fireParticles)
        {
            particles.Emit(1);
        }
    }
    
    private IEnumerator Run()
    {
        while (true)
        {
            if (!_navMeshAgent.isStopped && !_navMeshAgent.pathPending && Vector3.Distance ( transform.position , _navMeshAgent.pathEndPosition ) < _navMeshAgent.stoppingDistance)
            {
                _animator.SetBool(_runingId, false);
                _navMeshAgent.isStopped = true;
                yield break;
            }
            yield return null;
        }
    }
    
    IEnumerator CheckCanShoot()
    {
        while (true)
        {
            if (_navMeshAgent.isStopped || !_isAiming)
            {
                RaycastHit hit;
                Vector3 start = playerHeadTr.transform.TransformPoint(playerHeadTr.transform.localPosition);
                Vector3 end = enemyTr.transform.position;

                if (Physics.Raycast(start, end - start, out hit))
                {
                    if (hit.collider.CompareTag(EnemyTag))
                    {
                        _animator.SetBool(_aimingId, true);
                        _isAiming = true;
                        StartCoroutine(PlayerLookAtTarget());
                        enemyTr.GetComponentInParent<EnemyBehaviour>().MoveToPosition(transform.position);
                    }
                }
                yield break;
            }
            yield return null;
        }
    }
    
    IEnumerator PlayerLookAtTarget()
    {
        while (_isAiming)
        {
            var localTarget = transform.InverseTransformPoint(enemyTr.transform.position);
            var angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
            var eulerAngleVelocity = new Vector3(0, angle, 0);

            if (Mathf.Abs(angle) < 0.1)
            {
                StartCoroutine(EnableIKTwistConstraint());
                yield break;
            }

            var deltaRotation = Quaternion.Euler(eulerAngleVelocity * (Time.deltaTime * 10));

            _navMeshAgent.transform.rotation *= deltaRotation;
            yield return null;
        }
    }
    
    IEnumerator EnableIKTwistConstraint()
    {
        float i = 0.0f;
        while (true)
        {
            i += Time.deltaTime / 0.5f;
            twisterRig.weight = Mathf.Lerp(0, 1, i);

            if (i > 1f)
            {
                gunAimConstraint.weight = 1;
                _canShoot = true;
                yield break;
            }
            yield return null;
        }
    }

    IEnumerator WaitForShootingAnimationEnds()
    {
        float i = 0.0f; 
        float maxRecoilTime = Mathf.Min(0.4f / shootingSpeed, recoilTime); //todo get 0.4 animation time at runtime
        while (true)
        {
            i += Time.deltaTime/maxRecoilTime;
          
            gunRecoilRotationConstraint.weight = Mathf.Lerp(1, 0, i);

            if (_animator.GetCurrentAnimatorStateInfo(0).IsName(ShootingAnimation) &&
                (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f))
            {
                _canShoot = true;
                _shooting = false;
                _animator.SetBool(_shootingId, false);
                yield break;
            }

            yield return null;
        }
    }

}
