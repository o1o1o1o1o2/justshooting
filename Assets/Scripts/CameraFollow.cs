using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    
    public Transform target;

    private Transform _tr;
    private float _smoothSpeed = 0.125f;
    public Vector3 offset;

    private void Start()
    {
        Application.targetFrameRate = -1;
        QualitySettings.vSyncCount = 0;
        _tr = GetComponent<Transform>();
    }

    private Vector3 _targetPosition;
    
    private Vector3 velocity;
    
    void LateUpdate()
    {
        _targetPosition = target.position + offset;

        Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, _targetPosition, ref velocity, _smoothSpeed);
        transform.position = smoothedPosition;
        _tr.position = Vector3.Lerp(_tr.position, _targetPosition, _smoothSpeed);
    }
}
