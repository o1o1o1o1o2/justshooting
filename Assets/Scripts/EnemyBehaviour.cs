using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(SwitchChildRigidbodyKinematicType))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyBehaviour : MonoBehaviour
{
    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    private SwitchChildRigidbodyKinematicType _switchChildRigidbodyKinematicType;

    private Collider _collider;
    private Vector3 _force;
    private Vector3 _hit;
    private bool dead;

    private int _walkingId;

    private void Awake()
    {
        _switchChildRigidbodyKinematicType = GetComponent<SwitchChildRigidbodyKinematicType>();
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _walkingId = Animator.StringToHash("walk");
    }
    
    public void SetIdle()
    {
        dead = false;
        _animator.enabled = true;
        _navMeshAgent.enabled = true;
        _animator.SetBool(_walkingId, false);
    }

    public void Die(Collider collider, Vector3 force, Vector3 hit)
    {
        if (!dead)
        {
            _animator.SetBool(_walkingId, false);
            _navMeshAgent.ResetPath();
            _navMeshAgent.enabled = false;

            _animator.enabled = false;
            _switchChildRigidbodyKinematicType.MakeNotKinematic();
            _collider = collider;
            _force = force;
            _hit = hit;
            dead = true;
        }
        StartCoroutine(AddForce());
    }

    public void MoveToPosition(Vector3 position)
    {
        _navMeshAgent.SetDestination(position);
        _animator.SetBool(_walkingId, true);
    }

    private IEnumerator AddForce()
    {
        yield return new WaitForFixedUpdate();

        _collider.GetComponent<Rigidbody>().AddForceAtPosition(_force, _hit);
    }

}
