using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class respawn : MonoBehaviour
{
    
    private Transform parent;
    private Vector3 parentPosition;
    private Quaternion parentRotation;
    private Vector3[] positions;
    private Quaternion[] rotations;
    private Rigidbody[] rigidbodies;

    public void RestartRound()
    {
        LoadState();
    }

    void SaveState()
    {
        parentPosition = parent.position;
        parentRotation = parent.rotation;
        rigidbodies = parent.GetComponentsInChildren<Rigidbody>();
        positions = new Vector3[rigidbodies.Length];
        rotations = new Quaternion[rigidbodies.Length];
        for (int i = 0; i < rigidbodies.Length; i++)
        {
            positions[i] = rigidbodies[i].position;
            rotations[i] = rigidbodies[i].rotation;
        }
    }

    public void LoadState()
    {
        parent.position = parentPosition;
        parent.rotation = parentRotation;
        for (int i = 0; i < rigidbodies.Length; i++)
        {
            rigidbodies[i].Sleep();
            rigidbodies[i].position = positions[i];
            rigidbodies[i].rotation = rotations[i];
        }
    }

    private void Awake()
    {
        parent = transform;
    }

    void Start()
    {
        SaveState();
    }


}
